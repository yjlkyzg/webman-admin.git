<?php

namespace app\model;

/**
 * 文件上传记录
 */
class UploadFileModel extends BaseModel
{
    protected $table = 'upload_file';

    static function upload($data = [])
    {
        if (empty($data['unique_id'])) {
            return false;
        }

        if (self::where("unique_id", $data['unique_id'])->exists()) {
            return false;
        }
        
        $file = [
            'origin_name' => $data['origin_name'],
            'save_name' => $data['save_name'],
            'save_path' => $data['save_path'],
            'url' => $data['url'],
            'unique_id' => $data['unique_id'],
            'size' => $data['size'],
            'mime_type' => $data['mime_type'],
            'extension' => $data['extension'],
        ];

        self::create($file);

        return true;
    }
}
