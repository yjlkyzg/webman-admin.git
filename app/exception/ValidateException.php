<?php

namespace app\exception;

class ValidateException extends \Exception
{


    protected $message = '验证错误';

    public function __construct($message = '')
    {
        parent::__construct();

        if (!empty($message)) {
            $this->message = $message;
        }
    }
}
