/*
 Navicat Premium Data Transfer

 Source Server Type    : MySQL
 Source Server Version : 80024 (8.0.24)
 Source Schema         : webman_admin

 Target Server Type    : MySQL
 Target Server Version : 80024 (8.0.24)
 File Encoding         : 65001

 Date: 08/04/2023 15:14:45
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for wb_admin
-- ----------------------------
DROP TABLE IF EXISTS `wb_admin`;
CREATE TABLE `wb_admin` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `account` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `status` tinyint unsigned NOT NULL DEFAULT '1' COMMENT '1:正常 0:禁用',
  `roles` varchar(255) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '角色',
  `last_login_ip` varchar(255) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '最后登录ip',
  `last_login_time` bigint unsigned NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  `created_at` bigint unsigned NOT NULL DEFAULT '0',
  `updated_at` bigint unsigned NOT NULL DEFAULT '0',
  `deleted_at` bigint unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_account` (`account`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='管理员表';

-- ----------------------------
-- Records of wb_admin
-- ----------------------------
BEGIN;
INSERT INTO `wb_admin` (`id`, `account`, `password`, `status`, `roles`, `last_login_ip`, `last_login_time`, `created_at`, `updated_at`, `deleted_at`) VALUES (1, 'admin', '###450a1877e6bb5461fbe45c3add64f602', 1, '1', '125.33.170.90', 1680873540, 1680860389, 1680873540, NULL);
INSERT INTO `wb_admin` (`id`, `account`, `password`, `status`, `roles`, `last_login_ip`, `last_login_time`, `created_at`, `updated_at`, `deleted_at`) VALUES (2, 'test1', '###450a1877e6bb5461fbe45c3add64f602', 1, '2', '125.33.170.90', 1680919613, 1680916979, 1680919624, NULL);
COMMIT;

-- ----------------------------
-- Table structure for wb_admin_menu
-- ----------------------------
DROP TABLE IF EXISTS `wb_admin_menu`;
CREATE TABLE `wb_admin_menu` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `pid` int unsigned NOT NULL DEFAULT '0' COMMENT '上级菜单,顶级为0',
  `title` varchar(255) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '菜单名称',
  `icon` varchar(255) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '图标',
  `type` tinyint unsigned NOT NULL DEFAULT '0' COMMENT '类型0:目录 1:菜单',
  `app` varchar(255) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `controller` varchar(255) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `action` varchar(255) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `is_show` tinyint unsigned NOT NULL DEFAULT '1' COMMENT '是否显示 1:显示 0:隐藏',
  `sort` int unsigned NOT NULL DEFAULT '0' COMMENT '排序 大->小',
  `created_at` bigint unsigned NOT NULL DEFAULT '0',
  `updated_at` bigint unsigned NOT NULL DEFAULT '0',
  `deleted_at` bigint unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_search` (`action`,`controller`,`app`,`deleted_at`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='后台菜单表';

-- ----------------------------
-- Records of wb_admin_menu
-- ----------------------------
BEGIN;
INSERT INTO `wb_admin_menu` (`id`, `pid`, `title`, `icon`, `type`, `app`, `controller`, `action`, `is_show`, `sort`, `created_at`, `updated_at`, `deleted_at`) VALUES (1, 0, '系统管理', 'layui-icon-util', 0, 'admin', 'system', 'default', 1, 9999, 1680873247, 1680873247, NULL);
INSERT INTO `wb_admin_menu` (`id`, `pid`, `title`, `icon`, `type`, `app`, `controller`, `action`, `is_show`, `sort`, `created_at`, `updated_at`, `deleted_at`) VALUES (2, 1, '系统设置', '', 1, 'admin', 'system', 'setting', 1, 0, 1680873247, 1680873247, NULL);
INSERT INTO `wb_admin_menu` (`id`, `pid`, `title`, `icon`, `type`, `app`, `controller`, `action`, `is_show`, `sort`, `created_at`, `updated_at`, `deleted_at`) VALUES (3, 1, '菜单管理', '', 1, 'admin', 'system', 'menu', 1, 0, 1680873247, 1680873247, NULL);
INSERT INTO `wb_admin_menu` (`id`, `pid`, `title`, `icon`, `type`, `app`, `controller`, `action`, `is_show`, `sort`, `created_at`, `updated_at`, `deleted_at`) VALUES (4, 1, '角色管理', '', 1, 'admin', 'system', 'roles', 1, 0, 1680873247, 1680873247, NULL);
INSERT INTO `wb_admin_menu` (`id`, `pid`, `title`, `icon`, `type`, `app`, `controller`, `action`, `is_show`, `sort`, `created_at`, `updated_at`, `deleted_at`) VALUES (5, 1, '管理员管理', '', 1, 'admin', 'system', 'admin', 1, 0, 1680873247, 1680873247, NULL);
INSERT INTO `wb_admin_menu` (`id`, `pid`, `title`, `icon`, `type`, `app`, `controller`, `action`, `is_show`, `sort`, `created_at`, `updated_at`, `deleted_at`) VALUES (6, 3, '获取菜单列表', '', 1, 'admin', 'system', 'getMenuList', 0, 0, 1680877377, 1680877377, NULL);
INSERT INTO `wb_admin_menu` (`id`, `pid`, `title`, `icon`, `type`, `app`, `controller`, `action`, `is_show`, `sort`, `created_at`, `updated_at`, `deleted_at`) VALUES (7, 3, '添加/编辑 菜单', '', 1, 'admin', 'system', 'editMenu', 0, 0, 1680877377, 1680877377, NULL);
INSERT INTO `wb_admin_menu` (`id`, `pid`, `title`, `icon`, `type`, `app`, `controller`, `action`, `is_show`, `sort`, `created_at`, `updated_at`, `deleted_at`) VALUES (8, 3, '删除菜单', '', 1, 'admin', 'system', 'delMenu', 0, 0, 1680877377, 1680879827, NULL);
INSERT INTO `wb_admin_menu` (`id`, `pid`, `title`, `icon`, `type`, `app`, `controller`, `action`, `is_show`, `sort`, `created_at`, `updated_at`, `deleted_at`) VALUES (9, 4, '获取角色列表', '', 1, 'admin', 'system', 'getRolesList', 0, 0, 1680877377, 1680879827, NULL);
INSERT INTO `wb_admin_menu` (`id`, `pid`, `title`, `icon`, `type`, `app`, `controller`, `action`, `is_show`, `sort`, `created_at`, `updated_at`, `deleted_at`) VALUES (10, 4, '添加/编辑 角色', '', 1, 'admin', 'system', 'editRoles', 0, 0, 1680877377, 1680879827, NULL);
INSERT INTO `wb_admin_menu` (`id`, `pid`, `title`, `icon`, `type`, `app`, `controller`, `action`, `is_show`, `sort`, `created_at`, `updated_at`, `deleted_at`) VALUES (11, 4, '删除角色', '', 1, 'admin', 'system', 'delRoles', 0, 0, 1680877377, 1680879827, NULL);
INSERT INTO `wb_admin_menu` (`id`, `pid`, `title`, `icon`, `type`, `app`, `controller`, `action`, `is_show`, `sort`, `created_at`, `updated_at`, `deleted_at`) VALUES (12, 5, '获取管理员列表', '', 1, 'admin', 'system', 'getAdminList', 0, 0, 1680915841, 1680915841, NULL);
INSERT INTO `wb_admin_menu` (`id`, `pid`, `title`, `icon`, `type`, `app`, `controller`, `action`, `is_show`, `sort`, `created_at`, `updated_at`, `deleted_at`) VALUES (13, 5, '添加/编辑 管理员', '', 1, 'admin', 'system', 'editAdmin', 0, 0, 1680915871, 1680915871, NULL);
INSERT INTO `wb_admin_menu` (`id`, `pid`, `title`, `icon`, `type`, `app`, `controller`, `action`, `is_show`, `sort`, `created_at`, `updated_at`, `deleted_at`) VALUES (14, 5, '删除管理员', '', 1, 'admin', 'system', 'delAdmin', 0, 0, 1680915892, 1680915892, NULL);
INSERT INTO `wb_admin_menu` (`id`, `pid`, `title`, `icon`, `type`, `app`, `controller`, `action`, `is_show`, `sort`, `created_at`, `updated_at`, `deleted_at`) VALUES (15, 5, '修改管理员状态', '', 1, 'admin', 'system', 'updAdminStatus', 0, 0, 1680915921, 1680915921, NULL);
INSERT INTO `wb_admin_menu` (`id`, `pid`, `title`, `icon`, `type`, `app`, `controller`, `action`, `is_show`, `sort`, `created_at`, `updated_at`, `deleted_at`) VALUES (16, 0, '修改密码', '', 1, 'admin', 'system', 'editPassword', 0, 0, 1680918957, 1680919091, NULL);
INSERT INTO `wb_admin_menu` (`id`, `pid`, `title`, `icon`, `type`, `app`, `controller`, `action`, `is_show`, `sort`, `created_at`, `updated_at`, `deleted_at`) VALUES (17, 1, '上传附件列表', '', 1, 'admin', 'system', 'uploadFile', 1, 0, 1680937650, 1680937650, NULL);
INSERT INTO `wb_admin_menu` (`id`, `pid`, `title`, `icon`, `type`, `app`, `controller`, `action`, `is_show`, `sort`, `created_at`, `updated_at`, `deleted_at`) VALUES (18, 17, '获取附件列表', '', 1, 'admin', 'system', 'getUploadFile', 0, 0, 1680937674, 1680937674, NULL);
COMMIT;

-- ----------------------------
-- Table structure for wb_admin_roles
-- ----------------------------
DROP TABLE IF EXISTS `wb_admin_roles`;
CREATE TABLE `wb_admin_roles` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  `rules` text COLLATE utf8mb4_general_ci COMMENT '角色菜单ids',
  `created_at` bigint unsigned NOT NULL DEFAULT '0',
  `updated_at` bigint unsigned NOT NULL DEFAULT '0',
  `deleted_at` bigint unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='管理员角色表';

-- ----------------------------
-- Records of wb_admin_roles
-- ----------------------------
BEGIN;
INSERT INTO `wb_admin_roles` (`id`, `name`, `rules`, `created_at`, `updated_at`, `deleted_at`) VALUES (1, '超级管理员', '*', 1680863889, 1680863889, NULL);
INSERT INTO `wb_admin_roles` (`id`, `name`, `rules`, `created_at`, `updated_at`, `deleted_at`) VALUES (2, '角色管理员', '1,4,9,10,11,16', 1680863889, 1680919584, NULL);
COMMIT;

-- ----------------------------
-- Table structure for wb_option
-- ----------------------------
DROP TABLE IF EXISTS `wb_option`;
CREATE TABLE `wb_option` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `option_value` text COLLATE utf8mb4_general_ci,
  `created_at` bigint unsigned NOT NULL DEFAULT '0',
  `updated_at` bigint unsigned NOT NULL DEFAULT '0',
  `deleted_at` bigint unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of wb_option
-- ----------------------------
BEGIN;
INSERT INTO `wb_option` (`id`, `option_name`, `option_value`, `created_at`, `updated_at`, `deleted_at`) VALUES (1, 'site_config', '{\"title\":\"\\u540e\\u53f0\\u7ba1\\u7406\",\"desc\":\"\\u540e\\u53f0\\u7ba1\\u7406\\u7cfb\\u7edf\",\"logo\":\"\"}', 1680921711, 1680928537, NULL);
INSERT INTO `wb_option` (`id`, `option_name`, `option_value`, `created_at`, `updated_at`, `deleted_at`) VALUES (2, 'upload_config', '{\"type\":\"0\",\"extensions\":\"jpg,jpeg,png,gif,bmp4,mp4,avi,wmv,rm,rmvb,mkv,mp3,wma,wav,txt,pdf,doc,docx,xls,xlsx,ppt,pptx,zip,rar,csv\",\"maxsize\":\"10\"}', 1680923096, 1680928537, NULL);
COMMIT;

-- ----------------------------
-- Table structure for wb_upload_file
-- ----------------------------
DROP TABLE IF EXISTS `wb_upload_file`;
CREATE TABLE `wb_upload_file` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `origin_name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '原始文件名',
  `save_name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '保存文件名',
  `save_path` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '绝对路径',
  `url` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '访问路径',
  `unique_id` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'uniqid',
  `size` int unsigned NOT NULL DEFAULT '0' COMMENT '大小(字节)',
  `mime_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '类型',
  `extension` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '后缀',
  `created_at` bigint unsigned NOT NULL DEFAULT '0',
  `updated_at` bigint unsigned NOT NULL DEFAULT '0',
  `deleted_at` bigint unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_unique_id` (`unique_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='文件上传记录表';

-- ----------------------------
-- Records of wb_upload_file
-- ----------------------------
BEGIN;
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
